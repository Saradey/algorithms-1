//
//  main.cpp
//  Lesson1Algorithms
//
//  Created by Евгений Гончаров on 20/11/2018.
//  Copyright © 2018 Evgeny Goncharov. All rights reserved.
//

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

using namespace std;

int task2(int x, int y){
    return x > y ? x : y;
}

void swapTask3Teamp(int &x, int &y){ //передача по ссылке
    int teamp = x;
    x = y;
    y = teamp;
}

int task12(int x, int y, int z){
    int max = z;
    
    if(x > y && x > z)
        max = x;
    else if(y > z)
        max = y;
    
    return max;
}


int myRandTask13(){
    long long timeNow = time(0);

    timeNow = ((timeNow * 7621) + 1) % 100;
    
    return timeNow;
}


int main(int argc, const char * argv[]) {
    
    
    /////////
    //
    //Task 1
    //
    // I=m/(h*h); где m-масса тела в килограммах, h - рост в метрах.
    /////////
    cout <<"_________Task 1_________" << endl;
    
    float height, weight, result;
    cout << "Input height human, centimeters:" << endl;
    cin >> height;
    
    cout << "Input weight human, kilogram:" << endl;
    cin >> weight;
    
    result = weight / pow(height / 100, 2);
    cout << "Body mass index:" << result << endl;
    
     cout <<"_________end Task 1_________\n\n" << endl;
    /////////////
    
    
    
    /////////
    //
    //Task 2
    //
    // Найти максимальное из четырех чисел. Массивы не использовать.
    //TODO цикл?
    /////////
    cout <<"_________Task 2_________" << endl;
    int one, two, three, four;
    
    cout << "Input four number:" << endl;
    
    cin >> one >> two >> three >> four;
    int teamp1, teamp2;
    teamp1 = task2(one, two);
    teamp2 = task2(three, four);
    
    cout << "The biggest number:" << task2(teamp1, teamp2) << endl;
    
    cout <<"_________end Task 2_________\n\n";
    
    
    
    /////////
    //
    //Task 3
    //
    //TODO доделать задание с звездочкой
    /////////
    cout <<"_________Task 3_________" << endl;
    
    int x, y;
    
    cout << "Input two varibals please:" << endl;
    cout << "Введите x:" << endl;
    cin >> x;
    
    cout << "Введите y:" << endl;
    cin >> y;
    
    swapTask3Teamp(x, y);
    
    cout << "Результат после перестановки, x:" << x << " y:" << y << endl;
    
    cout <<"_________end Task 3_________\n\n";
    
    
    /////////
    //
    //Task 4
    //
    //
    /////////
    cout <<"_________Task 4_________" << endl;
    
    float a, b, c;
    
    cout << "Введите значения a, b, c" << endl;
    cin >> a >> b >> c;
    
    int D = pow(b, 2) - (4*a*c);
    
    cout << "D: " << D << endl;
    
    if(D < 0){
        cout << "Корней нет!" << endl;
    } else if(D == 0){
        cout << "Корень один" << endl;
        
    } else if (D > 0){
        cout << "Корней два" << endl;
        
        float x1, x2;
        
        x1 = ((b * -1) + sqrt(D)) / (2 * a);
        x2 = ((b * -1) - sqrt(D)) / (2 * a);
        cout << "x1:" << x1 << " x2:" << x2 << endl;
    }
    
    cout <<"_________end Task 4_________\n\n";
  
     
    /////////
    //
    //Task 5
    //
    //
    /////////
    
    cout <<"_________Task 5_________" << endl;
    
    short month;
    cout << "Введите месяц:" << endl;
    
    cin >> month;
    
    if(month > 12 || month < 1){
        cout << "такого месяца нет" << endl;
    } else{
        int result = month / 3;
        if(result == 4 || result == 0)
            cout << "Это зима" << endl; //12 1 2
        else if(result == 1)
            cout << "Это весна" << endl; //3 4 5
        else if(result == 2)
            cout << "Это лето"  << endl; //6 7 8
        else if(result == 3)
            cout << "Это осень" << endl; //9 10 11
    }
    
    cout <<"_________end Task 5_________\n\n";
    
    
    cout <<"_________Task 6_________" << endl;
    
    //1 21 31 41 51 61 101 121 год,
    //2 3 4 22 23 24 32 33 34 102 122 123 124 года,
    //5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 25 26 27 28 29 30 35 36 37 38 39 40 лет
    //111 112 113 114 115 116 117 118 119 120 125 лет
    
    int humanAge;
    
    cout << "Введите сколько лет человеку от 0 до 150:" << endl;
    cin >> humanAge;
    
    if(humanAge > 150 || humanAge <= 0){
        cout << "Ошибка" << endl;
    } else {
        
        if(humanAge % 10 == 1 && humanAge != 11 && humanAge != 111)
            cout << humanAge << " год" << endl;
        else if((humanAge % 10 == 2 || humanAge % 10 == 3 || humanAge % 10 == 4)  && humanAge != 112 &&
                humanAge != 113 && humanAge != 114 && humanAge != 12 && humanAge != 13 && humanAge != 14)
            cout << humanAge << " года" << endl;
        else cout << humanAge << " лет" << endl;
        
    }
    
    cout <<"_________end Task 6_________\n\n";

    
    

    cout <<"_________Task 7_________" << endl;
    int array7 [8][8];
    
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            array7[i][j] = 0;
        }
    }
    
    //0 белые 1 черные
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            
            if(j % 2 == 0)
                array7[i][j] = 0;
            else array7[i][j] = 1;
            
            if(i % 2 == 1){
                 array7[i][j] = 1;
                if(j % 2 != 0)
                    array7[i][j] = 0;
            }
            cout << array7[i][j] << " ";
        }
        cout << endl;
    }
    
    int x17, y17, x27, y27;
    cout << "Введите x1,y1, и x2, y2" << endl;
    cin >>x17 >> y17 >> x27 >> y27;
    
    if(array7[x17 - 1][y17 - 1] == array7[x27 - 1][y27 - 1])
        cout << "Поля одного цвета" << endl;
    else cout << "Поля разного цвета" << endl;
    
    cout <<"_________end Task 7_________\n\n";

     
     cout <<"_________Task 8_________" << endl;
    
    int x8, y8;
    
    cout << "Введите два числа:" << endl;
    cin >> x8 >> y8;
    
    if(x8 < 0 || y8 < 0)
    {
        cout << "Число не является натуральным" << endl;
    } else{
        cout << "x "<< x8 * x8 << " "<<x8*x8*x8 << endl;
        cout << "y "<< y8 * y8 << " "<<y8*y8*y8<< endl;
    }
    
    cout <<"_________end Task 8_________\n\n";

    

    cout <<"_________Task 9_________" << endl;
    
    int N, K;
    
    cout << "Введите N, и K:" << endl;
    cin >> N >> K;
    int result9 = 0;        //деление нацело
    
    while(true){
        if(K > N){
            K -= N;
            result9++;
        } else break;
    }
    cout << "Деление нацело " << result9 << endl;
    cout << "Остаток " << K << endl;
    
    cout << "_________end Task 9_________\n\n";

    

    cout <<"_________Task 10_________" << endl;
    
    cout << "Введите целое число:" << endl;
    int N10;
    
    cin >> N10;
    
    while(true){
        int teamp10;
        
        teamp10 = N10 % 10;
        if(teamp10 % 2 == 0){
            cout << "false" << endl;
        } else cout << "true" << endl;
        
        N10 /= 10;
        
        if(N10 < 10){
            if(N10 % 2 == 0){
                cout << "false" << endl;
            }else cout << "true" << endl;
            break;
        }
    }
    
    cout <<"_________end Task 10_________\n\n" << endl;

    
    
    cout <<"_________Task 11_________" << endl;
    
    int flag11;
    int number11 = 0;
    int count11 = 0;
    while(true){
        cout << "Введите число, для выхода введите 0" << endl;
        cin >> flag11;
        if(flag11 != 0){
            if((flag11 % 10) == 8){
                number11 +=flag11;
                count11++;
            }
        } else break;
    }
    int result11 = number11 / count11;
    cout << "Среднее арифмитическое:" << result11 << endl;
    
    cout <<"_________ end Task 11_________\n\n" << endl;


    
    cout <<"_________Task 12_________" << endl;
    
    int x12, y12, z12;
    
    cout << "Введите 3 числа:" << endl;
    cin >> x12 >> y12 >> z12;
    
    cout << "Максимальное число:" << task12(x12, y12, z12) << endl;
    
    cout <<"_________end Task 12_________" << endl;

    

    cout <<"_________Task 13_________" << endl;
    
    srand(time(nullptr));
    
    int result13 = rand() % 100;
    cout << "Рандомное число функция rand():" << result13 << endl;
    
    cout << "Рандомное число мое:" << myRandTask13() << endl;;
    
    cout <<"_________end Task 13_________\n\n" << endl;

    
    
    cout <<"_________Task 14_________" << endl;
    
    cout << "Введите натуральное число:" << endl;
    int N14;
    cin >> N14;
    
    if(N14 > 0){
        
        
        for(int i14 = 0; i14 <= N14; i14++){
            int flag14 = i14;
            int couteSup = 0;   //количество чисел
            
            while (true) {
                flag14 = flag14 / 10;
                if(flag14 == 0){
                    couteSup++;
                    break;
                }
                couteSup++;
            }
            
       
            int kvadrat = pow(i14, 2);
            int poslednie_chisla = pow(10, couteSup);   //если написать здесь, то компилятор ругается
            int poslednie_chisla_result = kvadrat % poslednie_chisla;
           
            if(poslednie_chisla_result == i14)
                cout << "Число " << i14 << " является автоморфным и попадает в диапазон " << N14 << endl;
         
            
            
        }
        
        
        
        
        
    } else cout << "Число не является натуральным" << endl;
    
    cout <<"_________endTask 14_________\n\n" << endl;
    
    
    
    system("PAUSE");
    return 0;
}



